package org.singularity.core.json;

import org.singularity.core.util.RemoteResource;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Author: harrynoob
 */
public final class ConfigLoader {

    private static final String CONFIG_LOCATION =
            System.getProperty("user.home") + File.separatorChar +
                    "Singularity" + File.separatorChar +
                    "config" + File.separatorChar;

    private static final String CONFIG_FILE_NAME = "resources.json";

    private static final String DEFAULT_CONFIG_SRC = "http://"; //TODO

    /*
    *  Configuration keys should be in the following format:
    *  {
    *  name: "NameOfTheResource", url: "URLOfResourceJar",
    *  info: "URLOfInfoPage", main: "FullClassName"
    *  }
    *  Where FullClassName is a fully specified class name
    *  (thus including package name, seperated by dots (e.g. org.singularity.core.Loader)).
    *  A config file should be formatted in the following way.
    *  { keys: [
    *       key1,
    *       key2
    *       keyn
    *  ] }
    *  Where keyn is described above.
     */
    public RemoteResource[] loadConfig() {
        String fileSource = readConfigFile();
        JSONObject base = new JSONObject(fileSource);
        JSONArray keys = base.getJSONArray("keys");
        RemoteResource[] resources = new RemoteResource[keys.length()];
        for(int i = 0; i < keys.length(); i++) {
            JSONObject jo = keys.getJSONObject(i);
            String name = jo.getString("name");
            String url = jo.getString("url");
            String info = jo.getString("info");
            String main = jo.getString("main");
            resources[i] = new RemoteResource(name, url, info, main);
        }
        throw new RuntimeException("No remote resources found. Exiting.");
    }

    private String readConfigFile() {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(CONFIG_LOCATION + CONFIG_FILE_NAME)
                        )
                )
        )) {
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            System.out.println("No config file found. Downloading default file.");
            if(downloadFile()) {
                return readConfigFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    private boolean downloadFile() {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new URL(DEFAULT_CONFIG_SRC).openStream()
                )
        ); FileWriter fw = new FileWriter(
                new File(CONFIG_LOCATION + CONFIG_FILE_NAME))
        ) {
            String line;
            while((line = br.readLine()) != null) {
                fw.write(line);
            }
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            throw new RuntimeException("Failed to download or save new config file", e);
        }
    }

    private static volatile ConfigLoader instance;

    private ConfigLoader() { }

    public static ConfigLoader getInstance() {
        if(instance == null) {
            synchronized (instance) {
                instance = new ConfigLoader();
            }
        }
        return instance;
    }

}
