package org.singularity.core;

import org.singularity.core.binding.Factory;
import org.singularity.core.forum.AccountDetails;
import org.singularity.core.forum.Forum;
import org.singularity.core.loader.Loader;
import org.singularity.core.ui.LoginScreen;

import java.applet.Applet;

/**
 * Author: harrynoob
 */
public final class Launcher {

    private Launcher() {}

    public static void main(String[] args) {
        Factory factory = Factory.getInstance();
        LoginScreen ls = factory.bind(LoginScreen.class).getImplInstance();
        Forum forum = factory.bind(Forum.class).getImplInstance();
        AccountDetails ad;
        while(!forum.verifyDetails(ad = ls.login())) {
            ls.showInvalidDetails();
        }

    }

}
