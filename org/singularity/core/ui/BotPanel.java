package org.singularity.core.ui;

import org.singularity.core.bot.Bot;

/**
 * Author: harrynoob
 */
public interface BotPanel {
    public boolean addBot(Bot bot);
    public void removeBot(Bot bot);
}
