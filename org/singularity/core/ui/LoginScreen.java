package org.singularity.core.ui;

import org.singularity.core.forum.AccountDetails;

/**
 * Author: harrynoob
 */
public interface LoginScreen {
    public AccountDetails login();
    public void showInvalidDetails();
}