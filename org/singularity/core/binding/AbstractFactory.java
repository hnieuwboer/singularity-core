package org.singularity.core.binding;

import org.singularity.core.util.ArrayUtilities;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: harrynoob
 */
public abstract class AbstractFactory {

    private Set<ClassBinding<?>> bindingSet = new HashSet<>();

    public AbstractFactory() {
        setup();
    }

    public <T> ClassBinding<T> bind(Class<T> clazzT) {
        for(ClassBinding<?> cb : bindingSet) {
            if(clazzT.equals(cb.getBind())) {
                @SuppressWarnings("unchecked")
                ClassBinding<T> toReturn = (ClassBinding<T>) cb;
                return toReturn;
            }
        }
        ClassBinding<T> classBinding = new ClassBinding<>(this, clazzT);
        bindingSet.add(classBinding);
        return classBinding;
    }

    public <T> T instantiate(Class<T> clazz, Object... params) {
        label: for(Constructor<?> constructor : clazz.getConstructors()) {
            if(constructor.isAnnotationPresent(Instantiable.class)) {
                Class<?>[] paramTypes = constructor.getParameterTypes();
                Object[] actualParams = new Object[paramTypes.length];
                for(int i = 0, paramsUsage = 0; i < actualParams.length; i++) {
                    ClassBinding<?> cb = bind(paramTypes[i]);
                    if(cb != null) {
                        actualParams[i] = cb.getImplInstance();
                    } else if(paramsUsage < params.length) {
                        actualParams[paramsUsage] = params[paramsUsage++];
                    } else {
                        continue label;
                    }
                }
            }
        }
        try {
            return instantiate(
                    clazz.getConstructor(
                            ArrayUtilities.convert(params, OBJECT_CLASS_CONVERTER).toArray(
                                    new Class<?>[params.length]
                            )
                    )
            );
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(
                    "No suitable constructor found for " + clazz.getName(),
                    e
            );
        }
    }

    private <T> T instantiate(Constructor<T> c, Object... params) {
        try {
            return c.newInstance(params);
        } catch (InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(
                    "Error while instantiating " + c.getName(),
                    e
            );
        }
    }

    protected abstract void setup();

    private static final ArrayUtilities.Converter<Object, Class<?>> OBJECT_CLASS_CONVERTER
            = new ArrayUtilities.Converter<Object, Class<?>>() {
        @Override
        public Class<?> convert(Object o) {
            return o.getClass();
        }
    };

}
