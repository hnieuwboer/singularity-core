package org.singularity.core.binding;

import org.singularity.core.util.Module;
import org.singularity.core.util.RemoteResource;
import org.singularity.core.json.ConfigLoader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author: harrynoob
 */
public final class Factory extends AbstractFactory {

    private static final Factory INSTANCE = new Factory();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(4);
    private Factory() {
    }

    public static Factory getInstance() {
        return INSTANCE;
    }

    @Override
    protected void setup() {
        for(final RemoteResource rr : ConfigLoader.getInstance().loadConfig()) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Module m = rr.getModule();
                    m.setupBindings(Factory.this);
                }
            };
            EXECUTOR_SERVICE.submit(r);
        }
    }
}
