package org.singularity.core.binding;

/**
 * Author: harrynoob
 */
public final class ClassBinding<T> {

    private final AbstractFactory baf;
    private final Class<T> toBind;
    private Class<? extends T> impl;
    private T implInstance;
    public ClassBinding(AbstractFactory baf, Class<T> toBind) {
        this.baf = baf;
        this.toBind = toBind;
    }

    public void to(Class<? extends T> clazz) {
        this.impl = clazz;
    }

    public T getImplInstance() {
        if(implInstance == null) {
            implInstance = baf.instantiate(impl);
        }
        return implInstance;
    }

    Class<T> getBind() {
        return toBind;
    }

}
