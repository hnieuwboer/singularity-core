package org.singularity.core.loader;

import java.applet.Applet;

/**
 * Author: harrynoob
 */
public interface Loader {
    public Applet load();
    public void shutdown();
}