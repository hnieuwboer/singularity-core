package org.singularity.core.util;

import org.singularity.core.binding.AbstractFactory;

/**
 * Author: harrynoob
 */
public interface Module {
    public void setupBindings(AbstractFactory baf);
}
