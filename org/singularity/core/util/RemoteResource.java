package org.singularity.core.util;

import org.singularity.core.binding.Factory;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Author: harrynoob
 */
public final class RemoteResource {

    private final String name;
    private final String url;
    private final String infoUrl;
    private final String main;
    private final RemoteClassLoader cl;
    public RemoteResource(String name, String url, String infoUrl, String main) {
        this.name = name;
        this.infoUrl = infoUrl;
        this.url = url;
        this.main = main;
        this.cl = new RemoteClassLoader(createUrl(this.url));
    }

    private URL createUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Module getModule() {
        cl.loadResources();
        try {
            return (Module) Factory.getInstance().instantiate(cl.loadClass(main));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Incorrect main class name specified for module " + name, e);
        }
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public String getMainClassName() {
        return main;
    }

}
