package org.singularity.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: harrynoob
 */
public final class ArrayUtilities {
    private ArrayUtilities() {}
    public static final <T, U> List<U> convert(T[] tArray, Converter<T, U> convertor) {
        List<U> list = new ArrayList<>();
        for(T t : tArray) {
            if(t != null) {
                U u = convertor.convert(t);
                if(u != null) {
                    list.add(u);
                }
            }
        }
        return list;
    }

    public static interface Converter<T, U> {
        public U convert(T t);
    }

}
