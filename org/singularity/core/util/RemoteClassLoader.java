package org.singularity.core.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * Author: harrynoob
 */
public final class RemoteClassLoader extends ClassLoader {

    private final URL url;
    private final Map<String, byte[]> resourceMap = new HashMap<>();
    public RemoteClassLoader(URL url) {
        this.url = url;
    }

    public void loadResources() {
        try (JarInputStream jis = new JarInputStream(url.openConnection().getInputStream())) {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] buf = new byte[4096];
                int read;
                while ((read = jis.read(buf, 0, buf.length)) != 0) {
                    bos.write(buf, 0, read);
                }
                resourceMap.put(je.getName(), bos.toByteArray());
            }
        } catch (IOException e) {
            throw new RuntimeException("IOException thrown while reading remote resource.", e);
        }
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String resourceName = name.replace('.', '/').concat(".class");
        byte[] bytes = resourceMap.get(resourceName);
        return bytes != null ?
                defineClass(resourceName, bytes, 0, bytes.length) :
                super.findClass(name);
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        byte[] bytes = resourceMap.get(name);
        if(bytes != null) {
            return new ByteArrayInputStream(bytes);
        }
        return super.getResourceAsStream(name);
    }

}
