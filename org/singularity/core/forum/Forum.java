package org.singularity.core.forum;

/**
 * Author: harrynoob
 */
public interface Forum {
    public boolean verifyDetails(AccountDetails ad);
}