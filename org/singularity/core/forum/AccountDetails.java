package org.singularity.core.forum;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Author: harrynoob
 */
public final class AccountDetails {

    private final String username;
    private final String passwordSHA;
    public AccountDetails(String username, char[] password) {
        this.username = username;
        this.passwordSHA = sha(password);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return passwordSHA;
    }

    private String sha(char[] password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] passwordBytes = new String(password).getBytes("UTF-8");
            byte[] sha = md.digest(passwordBytes);
            StringBuilder sb = new StringBuilder();
            for(byte b : sha) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

}
