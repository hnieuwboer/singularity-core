package org.singularity.core.bot;

import java.applet.Applet;
import java.awt.*;

/**
 * Author: harrynoob
 */
public interface Bot {
    public void start(Applet applet);
    public Canvas getCanvas();
}
