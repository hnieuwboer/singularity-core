package org.singularity.core.script;

/**
 * Author: harrynoob
 */
public interface Script {
    public ScriptGovernor getScriptGovernor();
}
