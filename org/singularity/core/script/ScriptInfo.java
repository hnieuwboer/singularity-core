package org.singularity.core.script;

/**
 * Author: harrynoob
 */
public interface ScriptInfo {
    public String getName();
    public String[] getAuthors();
    public double getVersion();
    public String getScriptClassName();
}
