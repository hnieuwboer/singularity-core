package org.singularity.core.script;

import org.singularity.core.forum.AccountDetails;

/**
 * Author: harrynoob
 */
public interface ScriptLoader {
    public ScriptInfo[] getAvailableScripts(AccountDetails ad);
    public Script loadScript(ScriptInfo si);
}
